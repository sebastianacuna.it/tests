var things = require('./basicrouter.js');
const PropertiesReader = require('properties-reader');
const prop = PropertiesReader('./properties/app.properties');


var compression = require('compression');
var express = require('Express');
var app = express();

getProperty = (a) => {return prop.get(a);}

//both index.js and things.js should be in same directory
app.use(getProperty('server.context-path'), things);

app.use(compression());

/* Try and cath only get synchronous functions */
try{

    app.listen(getProperty('server.port'), function () {
        console.log('Context Path: '+getProperty('server.context-path'));
        console.log('Port:  '+getProperty('server.port'));
        console.log("Running in (NODE_ENV):"  + process.env.NODE_ENV);
        console.log('App is running!!!');
    });

}catch(e){
    console.error(e);
}
  